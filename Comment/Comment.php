<?php
/**
 * Created by PhpStorm.
 * User: zw
 * Date: 2019/2/18
 * Time: 14:59
 */

namespace Comment;

use PDO;

class Comment
{
    private $pdo = '';
    private $appid = '';

    function __construct($appid)
    {
        $this->pdo = Db::connect();
        $this->appid = $appid;
    }

    public function getComment($pid)
    {
        $table = 'box';

        $sql = "SELECT content FROM {$table} WHERE appid={$this->appid} AND pid='$pid'";

        $res = false;
        foreach ($this->pdo->query($sql) as $row) {
            if (!$res) {
                $res = true;
            }
            self::box2Html(json_decode($row['content'], true));
        }

        return $res;
    }

    public function setComment($pid, $ip = 'unknown', $comment)
    {
        $table = 'box';

        $stmt = $this->pdo->prepare("SELECT pid,content FROM {$table} WHERE appid=:appid AND pid=:pid");
        $stmt->execute(array('appid' => $this->appid, 'pid' => $pid));
        if ($stmt->rowCount() > 0) {
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            $contentArr = json_decode($data['content'], true);
            $contentArr[$ip . time()] = $comment;
            $stmt = $this->pdo->prepare("UPDATE {$table} SET content=:content WHERE appid=:appid AND pid=:pid");
            $stmt->execute(array('content' => json_encode($contentArr), 'appid' => $this->appid, 'pid' => $pid));

            if (!$stmt->rowCount()) {
                echo "评论失败，请重试~";
            }
        } else {
            $stmt = $this->pdo->prepare("INSERT INTO {$table} (appid,pid,content) VALUES (:appid,:pid,:content)");
            $stmt->execute(array('appid' => $this->appid, 'pid' => $pid, 'content' => json_encode([$ip . time() => $comment])));

            if (!$stmt->rowCount()) {
                echo "评论失败，请重试~";
            }
        }
    }

    private function box2Html($arr)
    {
        echo "<ul class='boxul'>";
        foreach ($arr as $key => $val) {
            if (is_array($val)) {
                self::box2Html($val);
            } else {
                echo "<li class='boxli'><span class='boxip'>" . substr($key, 0, -10) . " :</span><span class='boxtext'>" . $val . "</span><span class='boxtime'>" . date('Y/m/d H:i:s', substr($key, -10)) . "</span></li>";
            }
        }
        echo "</ul>";
    }

    public function check($appsecret)
    {
        $table = "user";

        $stmt = $this->pdo->prepare("SELECT appsecret,website FROM {$table} WHERE appid=:appid");
        $stmt->execute(array('appid' => $this->appid));

        if ($stmt->rowCount() == 1) {
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($data['appsecret'] == $appsecret) {
                return array('website' => $data['website']);
            } else {
                $res = ['code' => '1244', 'data' => ['appid' => $this->appid, 'appsecret' => 'empty', 'website' => 'empty']];
                die(json_encode($res));
            }
        } else {
            $res = ['code' => '1444', 'data' => ['appid' => 'empty', 'appsecret' => 'empty', 'website' => 'empty']];
            die(json_encode($res));
        }
    }

    public function getAll()
    {
        $table = 'box';

        $sql = "SELECT id,pid FROM {$table}";
        echo "<ul class='boxul' style='margin-top: 30px'>";
        foreach ($this->pdo->query($sql) as $row) {
            echo "<li class='boxli'>{$row['id']} : <a href='index.php?pid={$row['pid']}'>{$row['pid']}</a></li>";
        }
        echo "</ul>";
    }
}
