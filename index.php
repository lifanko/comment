<?php
/**
 * Created by PhpStorm.
 * User: zw
 * Date: 2019/2/18
 * Time: 13:59
 */

namespace Comment;

require_once 'Comment/Db.php';
require_once 'Comment/Comment.php';

$config = json_decode(file_get_contents('config.json'));

$pid = Db::get('pid');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Comment</title>
    <style>
        #input {
            padding: 6px 10px;
            width: 500px;
            outline: none;
            border: 1px solid #6f6350;
            position: fixed;
            top: 0;
            height: 16px;
        }

        #submit {
            height: 30px;
            width: 80px;
            background-color: #d06365;
            color: whitesmoke;
            border: 1px solid #6f6350;
            position: fixed;
            top: 0;
            left: 529px;
            font-size: 14px;
        }

        .boxul {
            list-style: none;
            padding-left: 0;
        }

        .boxli {
            padding: 8px;
            border-bottom: 1px dotted #eee;
        }

        .boxip {
            font-weight: bolder;
            margin-right: 10px;
            color: #555;
        }

        .boxtext {
            font-weight: bolder;
            font-family: "Microsoft YaHei UI Light";
        }

        .boxtime {
            float: right;
            font-size: 12px;
            color: #555;
            margin-top: 2px;
        }
    </style>
</head>
<body>

<form action="index.php" method="get">
    <input type="hidden" name="pid" value="<?php echo $pid; ?>">
    <input type="text" name="comment" placeholder="在此处输入评论……" id="input" autocomplete="off">
    <input type="submit" value="确定" id="submit"">
</form>

<?php
$Comment = new Comment($config->appid);

if ($pid) {
    $website = $Comment->check($config->appsecret);

    // 判断是否有新评论，有则记录进数据库
    $new = Db::get('comment');
    if ($new) {
        $ip = Db::getIP();
        $comment = Db::get('comment');
        $Comment->setComment($pid, $ip, $comment);
    }

    echo "<div style='margin-top: 40px'><span style='position: absolute;top: 3px;right: 10px;color: #777'>网站许可：" . $website['website'] . "</span>";
    if (!$Comment->getComment($pid)) {
        echo "<p style=\"font-size: 24px;text-align: center;margin-top: 9%;font-weight: bolder;font-family: 'Microsoft YaHei UI Light'\">暂无评论，赶快观影评论抢沙发吧~</p>";
    }
    echo "</div>";
} else {
    if (isset($_GET['check']) && $_GET['check'] == 'lifanko') {
        $Comment->getAll();
    } else {
        echo "Empty";
    }
}
?>

</body>
</html>


